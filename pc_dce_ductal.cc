/*
 *  Copyright © 2009-2014 The Regents of the University of California.
 *  All Rights Reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  •   Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *  •   Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *  •   None of the names of any campus of the University of California, the name
 *      "The Regents of the University of California," or the names of any of its
 *      contributors may be used to endorse or promote products derived from this
 *      software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *  OF SUCH DAMAGE. 
 */     


#ifdef WIN32
extern "C" {
#include <getopt.h>
}
#else
#include <getopt.h>
#include <unistd.h>
#endif
#include <svkImageReaderFactory.h>
#include <svkImageReader2.h>
#include <svkImageWriter.h>
#include <svkImageWriterFactory.h>


using namespace svk;


int main (int argc, char** argv)
{

    string usemsg("\n") ; 
    usemsg += "Version \n";   
    usemsg += "pc_dce_ductal --adc adc_file_name --peak peak_ht_file_name             \n"; 
    usemsg += "               -o output_file_root                                     \n"; 
    usemsg += "                                                                       \n";  
    usemsg += "   --adc         adc_file_name       Name of ADC image                 \n"; 
    usemsg += "   --peak        peak_ht_file_name   Name of Peak Height image         \n";  
    usemsg += "   -o            output_file_root    Root name of output (no extension)\n";  
    usemsg += "   -v                                Verbose output.                   \n";
    usemsg += "   -h                                Print help mesage.                \n";  
    usemsg += "                                                                       \n";  
    usemsg += "Combines ADC and DCE Peak Height images, creating a Ductal map         \n";  
    usemsg += "                                                                       \n";  

    string adcFileName;
    string peakHtFileName; 
    string outputFileName; 
    bool   verbose = false;

    svkImageWriterFactory::WriterType dataTypeOut = svkImageWriterFactory::UNDEFINED;

    string cmdLine = svkProvenance::GetCommandLineString(argc, argv);

    enum FLAG_NAME {
        FLAG_FILE_1 = 0, 
        FLAG_FILE_2
    };

    static struct option long_options[] =
    {
        {"adc", required_argument, NULL,  FLAG_FILE_1},
        {"peak", required_argument, NULL,  FLAG_FILE_2},
        {0, 0, 0, 0}
    };

    /*
    *   Process flags and arguments
    */
    int i;
    int option_index = 0;
    while ((i = getopt_long(argc, argv, "o:hv", long_options, &option_index)) != EOF) {
        switch (i) {
            case FLAG_FILE_1:
                adcFileName.assign(optarg);
                break;
            case FLAG_FILE_2:
                peakHtFileName.assign(optarg);
                break;
            case 'o':
                outputFileName.assign(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            case 'h':
                cout << usemsg << endl;
                exit(0);  
                break;
            default:
                ;
        }
    }

    argc -= optind;
    argv += optind;

    if (argc != 0 || adcFileName.length() == 0 || peakHtFileName.length() == 0 || outputFileName.length() == 0) { 
        cout << usemsg << endl;
        exit(1); 
    }

    if (verbose) {
        cout << "ADC:     " << adcFileName << endl;
        cout << "PEAK HT: " << peakHtFileName << endl;
        cout << "OUTPUT:  " << outputFileName << endl;
    }

    svkImageReaderFactory* readerFactory = svkImageReaderFactory::New();
    svkImageReader2* readerADC           = readerFactory->CreateImageReader2(adcFileName.c_str());
    svkImageReader2* readerPeakHt        = readerFactory->CreateImageReader2(peakHtFileName.c_str());

    //  Get ADC reader type: 
    if (readerADC->IsA("svkDcmMriVolumeReader")) {
        if (verbose) {
            cout << "ADC Input DCM MRI " << endl;
        }
    } else if (readerADC->IsA("svkDcmEnhancedVolumeReader")) {
        if (verbose) {
            cout << "ADC Input DCM Enhanced MRI " << endl;
        }
    } else if (readerADC->IsA("svkIdfVolumeReader")) {
        svkIdfVolumeReader::SafeDownCast(readerADC)->SetReadIntAsSigned(true);
        if (verbose) {
            cout << "ADC Input IDF " << endl;
            cout << "Assuming ADC datatype is signed int." << endl;
        }
    }

    //  Get PeakHt reader type: 
    if (readerPeakHt->IsA("svkDcmMriVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::DICOM_MRI;
        if (verbose) {
            cout << "PeakHt Input DCM MRI " << endl;
        }
    } else if (readerPeakHt->IsA("svkDcmEnhancedVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::DICOM_ENHANCED_MRI;
        if (verbose) {
            cout << "PeakHt Input DCM Enhanced MRI " << endl;
        }
    } else if (readerPeakHt->IsA("svkIdfVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::IDF;
        if (verbose) {
            cout << "PeakHt Input IDF " << endl;
        }
    }

    readerFactory->Delete();

    //  Load ADC and PeakHt readers
    if (readerADC == NULL) {
        cerr << "Can not determine appropriate reader for: " << adcFileName << endl;
        exit(1);
    }
    readerADC->SetFileName(adcFileName.c_str());
    readerADC->Update();

    if (readerPeakHt == NULL) {
        cerr << "Can not determine appropriate reader for: " << peakHtFileName << endl;
        exit(1);
    }
    readerPeakHt->SetFileName(peakHtFileName.c_str());
    readerPeakHt->Update();

    // Create Ductal Math Objects
    svkMriImageData* ductal = svkMriImageData::New();
    readerPeakHt->GetOutput()->GetDcmHeader()->SetValue("SeriesDescription", "Ductal Map");
    ductal->ZeroCopy(readerPeakHt->GetOutput());
    ductal->GetProvenance()->SetApplicationCommand(cmdLine);

    vtkDataArray* adcArray    = vtkDataArray::SafeDownCast(readerADC->GetOutput()->GetPointData()->GetArray(0));
    vtkDataArray* peakArray   = vtkDataArray::SafeDownCast(readerPeakHt->GetOutput()->GetPointData()->GetArray(0));
    vtkDataArray* ductalArray = vtkDataArray::SafeDownCast(ductal->GetPointData()->GetArray(0));

    // Calculate Ductal Map
    int numVoxels[3];
    readerPeakHt->GetOutput()->GetNumberOfVoxels(numVoxels);
    int totalVoxels = numVoxels[0] * numVoxels[1] * numVoxels[2];

    double voxelValue;
    double adcValue;
    double peakValue;
    double ADC_SCALE    = 669.0 / 1000.0;
    double PEAK_SCALE   = 10.7 / 10.0;
    double ADD_CONSTANT = 448.0;

    for (int i = 0; i < totalVoxels; i++) {
        adcValue   = adcArray->GetTuple1(i);
        peakValue  = peakArray->GetTuple1(i);
        voxelValue = ADD_CONSTANT + ADC_SCALE * adcValue - PEAK_SCALE * peakValue;
        if (verbose) {
            cout << adcValue << " " << peakValue << " : " << voxelValue << endl;
        }
        ductalArray->SetTuple1(i, voxelValue);
    }
   
    // If the type is supported be svkImageWriterFactory then use it, otherwise use the vtkXMLWriter
    svkImageWriterFactory* writerFactory = svkImageWriterFactory::New();
    svkImageWriter*        writer        = static_cast<svkImageWriter*>(writerFactory->CreateImageWriter(dataTypeOut));

    if (writer == NULL) {
        cerr << "Can not determine writer of type: " << dataTypeOut << endl;
        exit(1);
    }

    writerFactory->Delete();
    writer->SetFileName(outputFileName.c_str());
    writer->SetInput(ductal);
    writer->Write();
    writer->Delete();

    readerADC->Delete();
    readerPeakHt->Delete();
    ductal->Delete();

    return 0; 
}
