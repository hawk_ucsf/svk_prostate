/*
 *  Copyright © 2009-2014 The Regents of the University of California.
 *  All Rights Reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  •   Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *  •   Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *  •   None of the names of any campus of the University of California, the name
 *      "The Regents of the University of California," or the names of any of its
 *      contributors may be used to endorse or promote products derived from this
 *      software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *  OF SUCH DAMAGE. 
 */     


#ifdef WIN32
extern "C" {
#include <getopt.h>
}
#else
#include <getopt.h>
#include <unistd.h>
#endif
#include <svkImageReaderFactory.h>
#include <svkImageReader2.h>
#include <svkImageWriter.h>
#include <svkImageWriterFactory.h>


using namespace svk;


int main (int argc, char** argv)
{

    string usemsg("\n") ; 
    usemsg += "Version \n";   
    usemsg += "pc_dce_cancer --t2 t2_file_name --slope slope_file_name -o output_file_root \n";
    usemsg += "               [--adc adc_file_name] [--scale dce_scale_factor]              \n"; 
    usemsg += "                                                                             \n";
    usemsg += "   --t2          t2_file_name        Name of T2 image                        \n"; 
    usemsg += "   --slope       slope_file_name     Name of DCE slope image                 \n";
    usemsg += "   --adc         adc_file_name       Name of optional ADC image              \n"; 
    usemsg += "   --scale       dce_scale_factor    Default = 100.0                         \n";
    usemsg += "   -o            output_file_root    Root name of output (no extension)      \n";  
    usemsg += "   -v                                Verbose output.                         \n";
    usemsg += "   -h                                Print help mesage.                      \n";  
    usemsg += "                                                                             \n";  
    usemsg += "   Calculates cancer probability map from ADC, T2 and Slope images:          \n";
    usemsg += "      L = -15.0 + 0.00130*T2 + 0.0100*ADC - 0.0419*Slope                     \n";
    usemsg += "      Probability = 1000 / (1 + exp(L))                                      \n";
    usemsg += "                                                                             \n";  

    string adcFileName;
    string slopeFileName; 
    string t2FileName; 
    string outputFileName; 
    double DCE_SCALE = 100.0;
    double dceScale  = DCE_SCALE;
    bool   verbose   = false;

    svkImageWriterFactory::WriterType dataTypeOut = svkImageWriterFactory::UNDEFINED;

    string cmdLine = svkProvenance::GetCommandLineString(argc, argv);

    enum FLAG_NAME {
        FLAG_FILE_1 = 0, 
        FLAG_FILE_2,
        FLAG_FILE_3,
        SCALE_FLAG
    };

    static struct option long_options[] =
    {
        {"t2", required_argument, NULL,  FLAG_FILE_1},
        {"slope", required_argument, NULL,  FLAG_FILE_2},
        {"adc", required_argument, NULL,  FLAG_FILE_3},
        {"scale", required_argument, NULL,  SCALE_FLAG},
        {0, 0, 0, 0}
    };

    /*
    *   Process flags and arguments
    */
    int i;
    int option_index = 0; 
    while ((i = getopt_long(argc, argv, "o:hv", long_options, &option_index)) != EOF) {
        switch (i) {
            case FLAG_FILE_1:
                t2FileName.assign(optarg);
                break;
            case FLAG_FILE_2:
                slopeFileName.assign(optarg);
                break;
            case FLAG_FILE_3:
                adcFileName.assign(optarg);
                break;
            case SCALE_FLAG:
                dceScale = atof(optarg);
                break;
            case 'o':
                outputFileName.assign(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            case 'h':
                cout << usemsg << endl;
                exit(0);  
                break;
            default:
                ;
        }
    }

    argc -= optind;
    argv += optind;

    if (argc != 0 || t2FileName.length() == 0 || slopeFileName.length() == 0 || outputFileName.length() == 0) { 
        cout << usemsg << endl;
        exit(1); 
    }

    if(verbose) {
        if (adcFileName.length() == 0)
        {
            cout << "ADC:   " << adcFileName << endl;
        }
        cout << "T2:    " << t2FileName << endl;
        cout << "SLOPE: " << slopeFileName << endl;
    }

    // Reader set-up
    svkImageReaderFactory* readerFactory = svkImageReaderFactory::New();
    svkImageReader2*       readerT2      = readerFactory->CreateImageReader2(t2FileName.c_str());
    svkImageReader2*       readerSlope   = readerFactory->CreateImageReader2(slopeFileName.c_str());
    svkImageReader2*       readerADC     = NULL;

    if (adcFileName.size() > 0) {
        readerADC = readerFactory->CreateImageReader2(adcFileName.c_str());
        if (readerADC->IsA("svkDcmMriVolumeReader")) {
            if (verbose) {
                cout << "ADC Input DCM MRI " << endl;
            }
        } else if (readerADC->IsA("svkDcmEnhancedVolumeReader")) {
            if (verbose) {
                cout << "ADC Input DCM Enhanced MRI " << endl;
            }
        } else if (readerADC->IsA("svkIdfVolumeReader")) {
            svkIdfVolumeReader::SafeDownCast(readerADC)->SetReadIntAsSigned(true);
            if (verbose) {
                cout << "ADC Input IDF " << endl;
                cout << "Assuming ADC datatype is signed int." << endl;
            }
        }
    }

    if (readerSlope->IsA("svkDcmMriVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::DICOM_MRI;
        if (verbose) {
            cout << "Slope Input DCM MRI " << endl;
        }
    } else if (readerSlope->IsA("svkDcmEnhancedVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::DICOM_ENHANCED_MRI;
        if (verbose) {
            cout << "Slope Input DCM Enhanced MRI " << endl;
        }
    } else if (readerSlope->IsA("svkIdfVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::IDF;
        if (verbose) {
            cout << "Slope Input IDF " << endl;
        }
    }

    if (readerT2->IsA("svkDcmMriVolumeReader")) {
        if(verbose) {
            cout << "Input DCM MRI " << endl;
        }
    } else if (readerT2->IsA("svkDcmEnhancedVolumeReader")) {
        if(verbose) {
            cout << "Input DCM Enhanced MRI " << endl;
        }
    } else if (readerT2->IsA("svkIdfVolumeReader")) {
        if(verbose) {
            cout << "Input IDF " << endl;
        }
    }

    readerFactory->Delete();

    //  Load readers
    if (readerT2 == NULL) {
        cerr << "Can not determine appropriate reader for: " << t2FileName << endl;
        exit(1);
    }
    readerT2->SetFileName(t2FileName.c_str());
    readerT2->Update();

    if (readerSlope == NULL) {
        cerr << "Can not determine appropriate reader for: " << slopeFileName << endl;
        exit(1);
    }
    readerSlope->SetFileName(slopeFileName.c_str());
    readerSlope->Update();

    if (readerADC != NULL) {
        readerADC->SetFileName(adcFileName.c_str());
        readerADC->Update();
    }

    // Calculate cancer probability
    svkMriImageData* cancer = svkMriImageData::New();
    readerT2->GetOutput()->GetDcmHeader()->SetValue("SeriesDescription", "Cancer Map");
    cancer->ZeroCopy(readerT2->GetOutput());
    cancer->GetProvenance()->SetApplicationCommand(cmdLine);

    vtkDataArray* adcArray     = NULL;
    vtkDataArray* slopeArray   = vtkDataArray::SafeDownCast(readerSlope->GetOutput()->GetPointData()->GetArray(0));
    vtkDataArray* T2Array      = vtkDataArray::SafeDownCast(readerT2->GetOutput()->GetPointData()->GetArray(0));
    vtkDataArray* cancerArray  = vtkDataArray::SafeDownCast(cancer->GetPointData()->GetArray(0));

    int numVoxels[3];
    readerSlope->GetOutput()->GetNumberOfVoxels(numVoxels);
    int totalVoxels = numVoxels[0] * numVoxels[1] * numVoxels[2];

    if (readerADC != NULL) {
        adcArray = vtkDataArray::SafeDownCast(readerADC->GetOutput()->GetPointData()->GetArray(0));

        double voxelValue;
        double tempValue;
        double adcValue;
        double T2Value;
        double slopeValue;
        double ADC_SCALE    = 0.01;
        double T2_SCALE     = 0.0013;
        double SLOPE_SCALE  = 0.0419 / dceScale;
        double ADD_CONSTANT = -15.0;

        for (int i = 0; i < totalVoxels; i++) {
            adcValue   = adcArray->GetTuple1(i);
            T2Value    = T2Array->GetTuple1(i);
            slopeValue = slopeArray->GetTuple1(i);
            tempValue  = ADD_CONSTANT + T2_SCALE * T2Value + ADC_SCALE * adcValue - SLOPE_SCALE * slopeValue;
            voxelValue = 1000.0 / (1.0 + exp(tempValue));
            if (verbose) {
                cout << T2Value << " " << adcValue << " " << slopeValue << " : " << voxelValue << endl;
            }
            cancerArray->SetTuple1(i, (int)voxelValue);
        }
    } else {
        double voxelValue;
        double tempValue;
        double T2Value;
        double slopeValue;
        double T2_SCALE     = 0.00158;
        double SLOPE_SCALE  = 0.0272 / dceScale;
        double ADD_CONSTANT = -1.37;

        for (int i = 0; i < totalVoxels; i++) {
            T2Value    = T2Array->GetTuple1(i);
            slopeValue = slopeArray->GetTuple1(i);
            tempValue  = ADD_CONSTANT + T2_SCALE * T2Value - SLOPE_SCALE * slopeValue;
            voxelValue = 1000.0 / (1.0 + exp(tempValue));
            if (verbose) {
                cout << T2Value << " " << slopeValue << " : " << voxelValue << endl;
            }
            cancerArray->SetTuple1(i, (int)(voxelValue));
        }
    }

    // If the type is supported be svkImageWriterFactory then use it, otherwise use the vtkXMLWriter
    svkImageWriterFactory* writerFactory = svkImageWriterFactory::New();
    svkImageWriter*        writer        = static_cast<svkImageWriter*>(writerFactory->CreateImageWriter(dataTypeOut));

    if (writer == NULL) {
        cerr << "Can not determine writer of type: " << dataTypeOut << endl;
        exit(1);
    }

    writerFactory->Delete();
    writer->SetFileName(outputFileName.c_str());
    writer->SetInput(cancer);
    writer->Write();
    writer->Delete();

    if (readerADC != NULL)
    {
        readerADC->Delete();
    }
    readerT2->Delete();
    readerSlope->Delete();
    cancer->Delete();

    return 0; 
}
