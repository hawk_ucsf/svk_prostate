/*
 *  Copyright © 2009-2014 The Regents of the University of California.
 *  All Rights Reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  •   Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *  •   Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *  •   None of the names of any campus of the University of California, the name
 *      "The Regents of the University of California," or the names of any of its
 *      contributors may be used to endorse or promote products derived from this
 *      software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *  OF SUCH DAMAGE. 
 */     


#ifdef WIN32
extern "C" {
#include <getopt.h>
}
#else
#include <getopt.h>
#include <unistd.h>
#endif
#include <svkImageReaderFactory.h>
#include <svkImageReader2.h>
#include <svkImageWriter.h>
#include <svkImageWriterFactory.h>


using namespace svk;


int main (int argc, char** argv)
{

    string usemsg("\n") ; 
    usemsg += "Version \n";   
    usemsg += "pc_diff_adc -i input_file_name -o output_file_root --b0 b0_vols          \n"; 
    usemsg += "                                                                         \n"; 
    usemsg += "                                                                         \n";  
    usemsg += "   -i      input_file_name     4D DICOM DWI image                        \n"; 
    usemsg += "   -o      output_file_root    Root name of output (no extension)        \n";
    usemsg += "   --b0    b0_vols             Number of T2 volumes at beginning of image\n";
    usemsg += "   --scale scale               Default = 1000000                         \n";  
    usemsg += "   -v                          Verbose output.                           \n";
    usemsg += "   -h                          Print help mesage.                        \n";  
    usemsg += "                                                                         \n";  
    usemsg += "Calculates ADC map from DWI image                                        \n";  
    usemsg += "                                                                         \n";  

    string inputFileName;
    string outputFileName; 
    bool   verbose     = false;
    int    b0vols      = 0;
    double scale       = 1000000;
    double SCALEFACTOR = -1.0/600.0;

    svkImageWriterFactory::WriterType dataTypeOut = svkImageWriterFactory::UNDEFINED;

    string cmdLine = svkProvenance::GetCommandLineString(argc, argv);

    enum FLAG_NAME {
        B0_FLAG = 0,
        SCALE_FLAG 
    };

    static struct option long_options[] =
    {
        {"b0vols", required_argument, NULL, B0_FLAG},
        {"scale", required_argument, NULL, SCALE_FLAG},
        {0, 0, 0, 0}
    };

    /*
    *   Process flags and arguments
    */
    int i;
    int option_index = 0;
    while ((i = getopt_long(argc, argv, "i:o:hv", long_options, &option_index)) != EOF) {
        switch (i) {
            case 'i':
                inputFileName.assign(optarg);
                break;
            case 'o':
                outputFileName.assign(optarg);
                break;
            case B0_FLAG:
                b0vols = atof(optarg);
                break;
            case SCALE_FLAG:
                scale = atof(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            case 'h':
                cout << usemsg << endl;
                exit(0);  
                break;
            default:
                ;
        }
    }

    argc -= optind;
    argv += optind;

    if (argc != 0 || inputFileName.length() == 0 || outputFileName.length() == 0) { 
        cout << usemsg << endl;
        exit(1); 
    }

    if (verbose) {
        cout << "INPUT:  " << inputFileName << endl;
        cout << "SKIP:   " << b0vols << endl;
        cout << "OUTPUT: " << outputFileName << endl;
    }

    svkImageReaderFactory* readerFactory = svkImageReaderFactory::New();
    svkImageReader2* reader              = readerFactory->CreateImageReader2(inputFileName.c_str());

    //  Get reader type: 
    if (reader->IsA("svkDcmMriVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::DICOM_MRI;
        if (verbose) {
            cout << "Input DCM MRI " << endl;
        }
    } else if (reader->IsA("svkDcmEnhancedVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::DICOM_ENHANCED_MRI;
        if (verbose) {
            cout << "Input DCM Enhanced MRI " << endl;
        }
    } else if (reader->IsA("svkIdfVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::IDF;
        svkIdfVolumeReader::SafeDownCast(reader)->SetReadIntAsSigned(true);
        if (verbose) {
            cout << "Input IDF " << endl;
            cout << "Assuming datatype is signed int." << endl;
        }
    }

    readerFactory->Delete();

    //  Load input
    if (reader == NULL) {
        cerr << "Can not determine appropriate reader for: " << inputFileName << endl;
        exit(1);
    }
    reader->SetFileName(inputFileName.c_str());
    reader->Update();

    // Set up B0 and geometric mean and final ADC volumes
    svkMriImageData* geoMean = svkMriImageData::New();
    svkMriImageData* b0      = svkMriImageData::New();
    svkMriImageData* ADC     = svkMriImageData::New();
    svkMriImageData::SafeDownCast(reader->GetOutput())->GetCellDataRepresentation()->GetImage(geoMean,
                                                                                              0,
                                                                                              "");
    svkMriImageData::SafeDownCast(reader->GetOutput())->GetCellDataRepresentation()->GetImage(b0,
                                                                                              0,
                                                                                              "");
    svkMriImageData::SafeDownCast(reader->GetOutput())->GetCellDataRepresentation()->GetImage(ADC,
                                                                                              0,
                                                                                              "ADC Map",
                                                                                              NULL,
                                                                                              0,
                                                                                              VTK_UNSIGNED_SHORT);
    ADC->GetProvenance()->SetApplicationCommand(cmdLine);

    vtkDataArray* geoMeanArray = vtkDataArray::SafeDownCast(geoMean->GetPointData()->GetArray(0));
    vtkDataArray* b0Array      = vtkDataArray::SafeDownCast(b0->GetPointData()->GetArray(0));
    vtkDataArray* adcArray     = vtkDataArray::SafeDownCast(ADC->GetPointData()->GetArray(0));

    // First calculate geometric mean
    int numVoxels[3];
    reader->GetOutput()->GetNumberOfVoxels(numVoxels);
    int totalVoxels = numVoxels[0] * numVoxels[1] * numVoxels[2];
    int numPhases   = reader->GetOutput()->GetPointData()->GetNumberOfArrays();
    int usedPhases  = numPhases - b0vols;

    if (verbose) {
        cout << "Phases:      " << numPhases << endl;
        cout << "Phases Used: " << usedPhases << endl;
    }

    double voxelValue;
    for (int i = 0; i < totalVoxels; i++) {
        vtkFloatArray* diffusionPhases = vtkFloatArray::SafeDownCast(
            svkMriImageData::SafeDownCast(reader->GetOutput())->GetCellDataRepresentation()->GetArray(i) 
       ); 
        float* dynamicVoxelPtr = diffusionPhases->GetPointer(0);
        double avgStack        = 1.0;
        
        if (verbose) {
            cout << "Phase Vals: ";
        }

        for (int pt = b0vols - 1; pt < numPhases; pt++) {
            if ( pt == b0vols - 1) {
                b0Array->SetTuple1(i, dynamicVoxelPtr[pt]);
            } else {
                avgStack *= dynamicVoxelPtr[pt];
                
                if (verbose) {
                    cout << " " << dynamicVoxelPtr[pt] << " ";
                }
            }    
        }

        voxelValue = pow(avgStack, 1.0/usedPhases);
        
        if (verbose) {
            cout << "Stack: " << avgStack << " Mean: " << voxelValue << endl;
        }

        geoMeanArray->SetTuple1(i, voxelValue);
    }

    // ADC calculation
    double geoMeanValue;
    double b0Value;
    for (int i = 0; i < totalVoxels; i++) {
        geoMeanValue = geoMeanArray->GetTuple1(i);
        b0Value      = b0Array->GetTuple1(i);

        voxelValue   = log(geoMeanValue / b0Value) * SCALEFACTOR * scale;

        if (voxelValue < 0 || isfinite(voxelValue) == 0) {
            voxelValue = 0;
        }

        if (verbose) {
            cout << "Geo: " << geoMeanValue << " B0: " << b0Value << " ADC: " << voxelValue << endl;
        }

        adcArray->SetTuple1(i, (int)(voxelValue));
    }
   
    // If the type is supported be svkImageWriterFactory then use it, otherwise use the vtkXMLWriter
    svkImageWriterFactory* writerFactory = svkImageWriterFactory::New();
    svkImageWriter*        writer        = static_cast<svkImageWriter*>(writerFactory->CreateImageWriter(dataTypeOut));

    if (writer == NULL) {
        cerr << "Can not determine writer of type: " << dataTypeOut << endl;
        exit(1);
    }

    writerFactory->Delete();
    writer->SetFileName(outputFileName.c_str());
    writer->SetInput(ADC);
    writer->Write();
    writer->Delete();

    reader->Delete();
    geoMean->Delete();

    return 0; 
}
