#
#	$URL$
#	$Rev$
#	$Author$
#	$Date$
#

-include /netopt/share/include/include.mk /usr/local/share/include/include.mk


DCMTK_DIR 	 = /netopt/dicom
VTK_DIR 	 = /netopt/lib/vtk-5.6
GL2PS        = "-DSVK_USE_GL2PS=ON"
CLAPACK_DIR  = /opt/src/freeware/clapack/clapack-3.2.1-CMAKE
ITK_DIR      = /netopt/InsightToolkit/InsightToolkit-4.4.1/lib/cmake/ITK-4.4                        

TRUNK_DIR				= ./
BUILD_DIR             	= $(TRUNK_DIR)/Build
RELEASE_BUILD_DIR     	= $(BUILD_DIR)/Release/$(ARCH)
DEBUG_BUILD_DIR       	= $(BUILD_DIR)/Debug/$(ARCH)
PROSTATE_BIN_DIR        = $(BINDIR)/prostate

PC_APPS = 					\
	pc_dce_cancer     		\
	pc_dce_ductal     		\
	pc_dce_high_grade     	\
	pc_dce_t1               \
	pc_diff_mean            \
	pc_diff_adc

.PHONY : clean 

deploy: build_all_local
	rrc_deploy.pl -a -d install

deploy_dev: build_all_local
	rrc_deploy.pl -a -d install_dev -t 

build_all_local: 
	mkdir -p $(RELEASE_BUILD_DIR); 
	(cd $(RELEASE_BUILD_DIR); pwd; cmake -DCMAKE_VERBOSE_MAKEFILE=ON -DCMAKE_BUILD_TYPE=Release -DDCMTK_DIR=${DCMTK_DIR} ${GL2PS} -DVTK_DIR=${VTK_DIR}  -DITK_DIR=${ITK_DIR} -DUCSF_INTERNAL=ON -DBUILD_APPS=ON -DGETOPT_LIB=${GETOPT_LIB} -DBUILD_CREATE_RAW=ON -DBUILD_CLAPACK=ON -DCLAPACK_DIR=${CLAPACK_DIR} -DBUILD_ITK=ON  -DSVK_RELEASE_VERSION=1.0 ../../../; pwd; make -j4)

install: 
	for app in $(PC_APPS); do $(INSTALL) -m 775 $(RELEASE_BUILD_DIR)/$${app} $(PROSTATE_BIN_DIR)/$${app}; done

install_dev:
	for app in $(PC_APPS); do $(INSTALL) -m 775 $(RELEASE_BUILD_DIR)/$${app} $(PROSTATE_BIN_DIR)/$${app}.dev; done


docs:
	for app in $(PC_APPS); do $(RELEASE_BUILD_DIR)/$${app} -h > $${app}.txt; done 


-include /netopt/share/include/deps.mk /usr/local/share/include/deps.mk

