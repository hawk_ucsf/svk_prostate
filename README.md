Tools to create maps from prostate exam data, built using C++/VTK based [Sivic](https://github.com/SIVICLab/sivic) framework.

# Maps include:
- T1 Map: Dual flip-angle T1 calculation
- ADC Map: Apparent Diffusion Coeficient map, from 4D DWI image
- Average Diffusion Map: Average and geometric mean maps, from 4D DWI image 
- Cancer Probability Map: derived from T2, DCE up slope, and ADC
- High-Grade Map: derived from T2, DCE washout, and ADC
- Ductal Map: derived from ADC and DCE peak enhancement

