cmake_minimum_required(VERSION 2.6)

PROJECT(testing)


FIND_PATH(DCMTK_DIR dctk.h)


#
#   Set build dir:
#
SET(PATHROOT /netopt)
IF (CMAKE_SYSTEM_NAME MATCHES Linux)
    IF (CMAKE_SYSTEM_PROCESSOR MATCHES i686)
        SET(EXECUTABLE_OUTPUT_PATH ../Linux_i686)
        SET(LIBRARY_OUTPUT_PATH ../Linux_i686)
    ELSE (CMAKE_SYSTEM_PROCESSOR MATCHES i686)
        SET(EXECUTABLE_OUTPUT_PATH ../Linux_x86_64)
        SET(LIBRARY_OUTPUT_PATH ../Linux_x86_64)
    ENDIF (CMAKE_SYSTEM_PROCESSOR MATCHES i686)
ENDIF (CMAKE_SYSTEM_NAME MATCHES Linux)


IF (CMAKE_BUILD_TYPE MATCHES Debug)
    SET(EXECUTABLE_OUTPUT_PATH ${EXECUTABLE_OUTPUT_PATH}_debug)
    SET(LIBRARY_OUTPUT_PATH ${LIBRARY_OUTPUT_PATH}_debug)
ENDIF(CMAKE_BUILD_TYPE MATCHES Debug)

INCLUDE(${CMAKE_ROOT}/Modules/FindOpenGL.cmake)
INCLUDE(${CMAKE_ROOT}/Modules/FindGLU.cmake)
INCLUDE(${CMAKE_ROOT}/Modules/FindVTK.cmake)
IF (USE_VTK_FILE)
    INCLUDE(${USE_VTK_FILE})
ENDIF (USE_VTK_FILE)

ADD_DEFINITIONS(-DHAVE_CONFIG_H) 



LINK_DIRECTORIES(
    ${LIBRARY_OUTPUT_PATH}
    #/home/jhawkins/sivic/libs/Linux_x86_64_debug  # Local sivuc dev build
    /netopt/lib/local/dev  # Sivic dev build
    /netopt/share/include/svk/dev/  # Sivic dev build
    ${PATHROOT}/lib/local
    ${PATHROOT}/dicom/lib
)


INCLUDE_DIRECTORIES(
    ./
    #/home/jhawkins/sivic/libs/Linux_x86_64_debug  # Local sivuc dev build
    /netopt/lib/local/dev  # Sivic dev build
    /netopt/share/include/svk/dev/  # Sivic dev build
    ${PATHROOT}/share/include
    ${PATHROOT}/share/include/svk
    ${PATHROOT}/share/include/dicom/dev 
    ${PATHROOT}/dicom/include/dcmtk/dcmdata
    ${PATHROOT}/dicom/include/dcmtk/ofstd
    ${PATHROOT}/include/dcmtk/dcmdata
    ${PATHROOT}/include/dcmtk/ofstd
)

# if dcmtk 3.6, need to add liboflog:
SET(DCMTK_OFLOG "")
if ( EXISTS "${DCMTK_DIR}/lib/liboflog.a" OR EXISTS "${DCMTK_DIR}/lib/oflog.lib")
    SET(DCMTK_OFLOG oflog )
endif (EXISTS "${DCMTK_DIR}/lib/liboflog.a" OR EXISTS "${DCMTK_DIR}/lib/oflog.lib")

###################################
#   ITK 
###################################
ADD_DEFINITIONS(-DITK_BUILD)
FIND_PACKAGE( ITK REQUIRED )
IF(ITK_FOUND)
    INCLUDE(${ITK_USE_FILE})
ELSE(ITK_FOUND)
    MESSAGE(FATAL_ERROR "Cannot build without ITK.  Please set ITK_DIR.")
ENDIF(ITK_FOUND)
LINK_DIRECTORIES( ${ITK_DIR}/lib )


SET(MRK_LIBS
    ${CMINPACK_LIBS}
    svk 
    svkadapt 
    svkPSD
    svkTypeUtils
    vtkImaging
    vtkIO
    vtkFiltering
    vtkCommon
    dcmdata
    ITKDICOMParser
    ${DCMTK_OFLOG}
    ofstd 
    z
    ${PLATFORM_LIBS}
    ${CMINPACK_LIBS_PRE}
)


ADD_EXECUTABLE(
    pc_dce_cancer 
    pc_dce_cancer.cc
)

TARGET_LINK_LIBRARIES(
    pc_dce_cancer 
	${MRK_LIBS}
)

ADD_EXECUTABLE(
    pc_dce_ductal 
    pc_dce_ductal.cc
)

TARGET_LINK_LIBRARIES(
    pc_dce_ductal 
    ${MRK_LIBS}
)

ADD_EXECUTABLE(
    pc_dce_high_grade 
    pc_dce_high_grade.cc
)

TARGET_LINK_LIBRARIES(
    pc_dce_high_grade 
    ${MRK_LIBS}
)

ADD_EXECUTABLE(
    pc_dce_t1 
    pc_dce_t1.cc
)

TARGET_LINK_LIBRARIES(
    pc_dce_t1 
    ${MRK_LIBS}
)

ADD_EXECUTABLE(
    pc_diff_mean 
    pc_diff_mean.cc
)

TARGET_LINK_LIBRARIES(
    pc_diff_mean 
    ${MRK_LIBS}
)

ADD_EXECUTABLE(
    pc_diff_adc 
    pc_diff_adc.cc
)

TARGET_LINK_LIBRARIES(
    pc_diff_adc 
    ${MRK_LIBS}
)
