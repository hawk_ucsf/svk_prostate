/*
 *  Copyright © 2009-2014 The Regents of the University of California.
 *  All Rights Reserved.
 *
 *  Redistribution and use in source and binary forms, with or without
 *  modification, are permitted provided that the following conditions are met:
 *  •   Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *  •   Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *  •   None of the names of any campus of the University of California, the name
 *      "The Regents of the University of California," or the names of any of its
 *      contributors may be used to endorse or promote products derived from this
 *      software without specific prior written permission.
 *
 *  THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND
 *  ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED
 *  WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
 *  IN NO EVENT SHALL THE COPYRIGHT HOLDERS OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT,
 *  INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT
 *  NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 *  PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
 *  WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) 
 *  ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY
 *  OF SUCH DAMAGE. 
 */     


#ifdef WIN32
extern "C" {
#include <getopt.h>
}
#else
#include <getopt.h>
#include <unistd.h>
#endif
#include <svkImageReaderFactory.h>
#include <svkImageReader2.h>
#include <svkImageWriter.h>
#include <svkImageWriterFactory.h>
#define  PI 3.14159265359


using namespace svk;


int main (int argc, char** argv)
{

    string usemsg("\n") ; 
    usemsg += "Version \n";   
    usemsg += "pc_dce_t1 --pre pre_contrast_image --post post_contrast_image        \n";
    usemsg += "          [--pre_t pre_time_point] [--post_t post_time_point]        \n";
    usemsg += "          [--avg post_averages] [--tr tr]                            \n";
    usemsg += "          [--ang_pre angle_pre_image] [--ang_post angle_post_image]  \n";
    usemsg += "          [--r1_pre r1_pre_image]     [--r2_pre r1_pre_image]        \n";
    usemsg += "          [--r1_post r1_post_image]   [--r2_post r1_post_image]      \n";
    usemsg += "          -o output_file_root                                        \n"; 
    usemsg += "                                                                          \n"; 
    usemsg += "   --pre       pre_contrast_image  Name of pre-contrast image             \n";
    usemsg += "   --pre_t     pre_time_point      If the pre-contrast image input is 4d, \n";
    usemsg += "                                   the specific volume to use             \n";
    usemsg += "   --post      post_contrast_image Name of post-contrast image            \n";
    usemsg += "   --post_t    post_time_point     If the post-contrast image input is 4d,\n";
    usemsg += "                                   the specific volume to use             \n";
    usemsg += "   --avg       post_averages       Default = 1                            \n";
    usemsg += "   --tr        tr                  Default = 3.5                          \n";
    usemsg += "   --ang_pre   angle_pre_image     Default = 10.0 degrees                 \n";
    usemsg += "   --ang_post  angle_post_image    Default = 5.0  degrees                 \n"; 
    usemsg += "   --r1_pre    r1_pre_image        Default = 13.0                         \n";
    usemsg += "   --r2_pre    r2_pre_image        Default = 30.0                         \n";
    usemsg += "   --r1_post   r1_post_image       Default = 13.0                         \n";
    usemsg += "   --r2_post   r2_post_image       Default = 30.0                         \n";
    usemsg += "   -o          output_file_root    Root name of output (no extension)     \n";  
    usemsg += "   -v                              Verbose output.                        \n";
    usemsg += "   -h                              Print help mesage.                     \n";  
    usemsg += "                                                                          \n";  

    string preFileName;
    string postFileName;
    string outputFileName;
    int    AVG      = 1;
    double TR       = 3.5; 
    double ANG1_DEG = 10.0;
    double ANG2_DEG = 5.0;
    double ANG1     = ANG1_DEG * PI / 180.0;
    double ANG2     = ANG2_DEG * PI / 180.0;
    double R1       = 13.0;
    double R2       = 30.0;
    double MAXT1    = 10000;

    int    preVol  = 1;
    int    postVol = 1;
    int    avgNum  = AVG;
    double tr      = TR;
    double angPre  = ANG1;
    double angPost = ANG2;
    double r1Pre   = R1;
    double r2Pre   = R2;
    double r1Post  = R1;
    double r2Post  = R2;
    bool   verbose = false;

    svkImageWriterFactory::WriterType dataTypeOut = svkImageWriterFactory::UNDEFINED;

    string cmdLine = svkProvenance::GetCommandLineString(argc, argv);

    enum FLAG_NAME {
        FLAG_FILE_1 = 0,
        FLAG_PRE_TP,
        FLAG_FILE_2,
        FLAG_POST_TP,
        FLAG_AVG,
        FLAG_TR,
        FLAG_ANG_PRE,
        FLAG_ANG_POST,
        FLAG_R1_PRE,
        FLAG_R2_PRE,
        FLAG_R1_POST,
        FLAG_R2_POST,
    };

    static struct option long_options[] =
    {
        {"pre", required_argument, NULL,  FLAG_FILE_1},
        {"pre_t", required_argument, NULL,  FLAG_PRE_TP},
        {"post", required_argument, NULL,  FLAG_FILE_2},
        {"post_t", required_argument, NULL,  FLAG_POST_TP},
        {"avg", required_argument, NULL,  FLAG_AVG},
        {"tr", required_argument, NULL,  FLAG_TR},
        {"ang_pre", required_argument, NULL,  FLAG_ANG_PRE},
        {"ang_post", required_argument, NULL,  FLAG_ANG_POST},
        {"r1_pre", required_argument, NULL,  FLAG_R1_PRE},
        {"r2_pre", required_argument, NULL,  FLAG_R2_PRE},
        {"r1_post", required_argument, NULL,  FLAG_R1_POST},
        {"r2_post", required_argument, NULL,  FLAG_R2_POST},
        {0, 0, 0, 0}
    };


    /*
    *   Process flags and arguments
    */
    int i;
    int option_index = 0; 
    while ((i = getopt_long(argc, argv, "o:hv", long_options, &option_index)) != EOF) {
        switch (i) {
            case FLAG_FILE_1:
                preFileName.assign(optarg);
                break;
            case FLAG_PRE_TP:
                preVol = atof(optarg);
                break;
            case FLAG_FILE_2:
                postFileName.assign(optarg);
                break;
            case FLAG_POST_TP:
                postVol = atof(optarg);
                break;
            case FLAG_AVG:
                avgNum = atof(optarg);
                break;
            case FLAG_TR:
                tr = atof(optarg);
                break;
            case FLAG_ANG_PRE:
                angPre = atof(optarg) * PI / 180.0;
                break;
            case FLAG_ANG_POST:
                angPost = atof(optarg) * PI / 180.0;
                break;
            case FLAG_R1_PRE:
                r1Pre = atof(optarg);
                break;
            case FLAG_R2_PRE:
                r2Pre = atof(optarg);
                break;
            case FLAG_R1_POST:
                r1Post = atof(optarg);
                break;
            case FLAG_R2_POST:
                r2Post = atof(optarg);
                break;
            case 'o':
                outputFileName.assign(optarg);
                break;
            case 'v':
                verbose = true;
                break;
            case 'h':
                cout << usemsg << endl;
                exit(0);  
                break;
            default:
                ;
        }
    }

    argc -= optind;
    argv += optind;

    if (argc != 0 || preFileName.length() == 0 || postFileName.length() == 0 || outputFileName.length() == 0) { 
        cout << usemsg << endl;
        exit(1); 
    }

    if (verbose) {
        cout << "Pre:  " << preFileName << endl;
        cout << "Post: " << postFileName << endl;
    }

    // Image reader set-up
    svkImageReaderFactory* readerFactory = svkImageReaderFactory::New();
    svkImageReader2*       readerPre     = readerFactory->CreateImageReader2(preFileName.c_str());
    svkImageReader2*       readerPost    = readerFactory->CreateImageReader2(postFileName.c_str());

    if (readerPre->IsA("svkDcmMriVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::DICOM_MRI;
        if (verbose) {
            cout << "Pre Input DCM MRI " << endl;
        }
    } else if (readerPre->IsA("svkDcmEnhancedVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::DICOM_ENHANCED_MRI;
        if (verbose) {
            cout << "Pre Input DCM Enhanced MRI " << endl;
        }
    } else if (readerPre->IsA("svkIdfVolumeReader")) {
        dataTypeOut = svkImageWriterFactory::IDF;
        if (verbose) {
            cout << "Pre Input IDF " << endl;
        }
    }

    if (readerPost->IsA("svkDcmMriVolumeReader")) {
        if(verbose) {
            cout << "Post Input DCM MRI " << endl;
        }
    } else if (readerPost->IsA("svkDcmEnhancedVolumeReader")) {
        if(verbose) {
            cout << "Post Input DCM Enhanced MRI " << endl;
        }
    } else if (readerPost->IsA("svkIdfVolumeReader")) {
        if(verbose) {
            cout << "Post Input IDF " << endl;
        }
    }

    readerFactory->Delete();

    // Load readers
    if (readerPre == NULL) {
        cerr << "Can not determine appropriate reader for: " << preFileName << endl;
        exit(1);
    }
    readerPre->SetFileName(preFileName.c_str());
    readerPre->Update();

    if (readerPost == NULL) {
        cerr << "Can not determine appropriate reader for: " << postFileName << endl;
        exit(1);
    }
    readerPost->SetFileName(postFileName.c_str());
    readerPost->Update();

    int numPreVols  = readerPre->GetOutput()->GetPointData()->GetNumberOfArrays();
    int numPostVols = readerPost->GetOutput()->GetPointData()->GetNumberOfArrays();
    if(preVol < 1 || postVol < 1) {
        cerr << "Timepoints must be greater than 1" << endl;
        cerr << "Pre-contrast timepoint input:  " << preVol << endl;
        cerr << "Post-contrast timepoint input: " << postVol << endl;
        exit(1);
    }
    if(preVol > numPreVols || postVol > numPostVols) {
        cerr << "Timepoint input greater than number of volumes in image" << endl;
        cerr << "Pre-contrast timepoint input:    " << preVol << endl;
        cerr << "Actual pre-contrast timepoints:  " << numPreVols << endl;
        cerr << "Post-contrast timepoint input:   " << postVol << endl;
        cerr << "Actual post-contrast timepoints: " << numPostVols << endl;
        exit(1);
    }
    if(avgNum < 1 || avgNum > (numPostVols - postVol + 1)) {
        cerr << "Please input valid number of post-contrast volumes to average" << endl;
        cerr << "Post-contrast averages input:    " << avgNum << endl;
        cerr << "Post-contrast timepoint input:   " << postVol << endl;
        cerr << "Actual post-contrast timepoints: " << numPostVols << endl;
        exit(1);
    }

    // Create DFA T1 Map
    svkMriImageData* T1 = svkMriImageData::New();
    readerPre->GetOutput()->GetDcmHeader()->SetValue("SeriesDescription", "T1 Map");
    T1->ZeroCopy(readerPre->GetOutput());
    T1->GetProvenance()->SetApplicationCommand(cmdLine);

    int vtkDataType = T1->GetPointData()->GetArray(0)->GetDataType();

    vtkDataArray* postArray;
    vtkDataArray* preArray = vtkDataArray::SafeDownCast(readerPre->GetOutput()->GetPointData()->GetArray(preVol-1));
    vtkDataArray* T1Array  = vtkDataArray::SafeDownCast(T1->GetPointData()->GetArray(0));
    vtkDataArray* avgArray = vtkDataArray::CreateDataArray(vtkDataType);

    int numVoxels[3];
    readerPre->GetOutput()->GetNumberOfVoxels(numVoxels);
    int    totalVoxels = numVoxels[0] * numVoxels[1] * numVoxels[2];
    double gainPre     = pow(10.0, (R1 - r1Pre) / 20.0) * pow(2.0, (R2 - r2Pre));
    double gainPost    = pow(10.0, (R1 - r1Post) / 20.0) * pow(2.0, (R2 - r2Post));
    if (verbose) {
        cout << "Pre-gain:  " << gainPre << endl;
        cout << "Post-gain: " << gainPre << endl;
    }

    avgArray->SetNumberOfComponents(1);
    avgArray->SetNumberOfTuples(totalVoxels);
    avgArray->SetName("average");

    for (int i = 0; i < totalVoxels; i++) {
        avgArray->SetTuple1(i, 0.0);
    }
    
    double avgValue;
    double postValue;
    for (int vol = (postVol - 1); vol < numPostVols; vol++) {
        postArray = vtkDataArray::SafeDownCast(readerPost->GetOutput()->GetPointData()->GetArray(vol));
        for (int i = 0; i < totalVoxels; i++) {
            avgValue  = avgArray->GetTuple1(i);
            postValue = postArray->GetTuple1(i);
            avgValue += postValue * gainPost;
            avgArray->SetTuple1(i, avgValue);
        }
    }

    double voxelValue;
    double tempValue;
    double denValue;
    double numValue;
    double ratio;
    double preValue;
    bool   error = false;

    for (int i = 0; i < totalVoxels; i++) {
        avgValue  = avgArray->GetTuple1(i);
        avgValue /= avgNum;
        preValue  = preArray->GetTuple1(i);
        postValue = postArray->GetTuple1(i);
        numValue  = preValue * gainPre * sin(angPost) - avgValue * sin(angPre);
        denValue  = preValue * gainPre * cos(angPre) * sin(angPost) - avgValue * sin(angPre) * cos(angPost);
        
        if (denValue == 0)
        {
            denValue = 1.0;
            error    = true;
        }

        ratio = numValue / denValue;
        if (ratio < 0)
        {
            ratio = 1.0;
            error = true;
        } else if (ratio == 0)
        {
            ratio = 1.0;
        }

        tempValue = log(ratio);
        if (tempValue == 0)
        {
            tempValue = 1.0;
            error     = true;
        }

        if (((-1.0 * tr / tempValue) > 0) && ((-1.0 * tr / tempValue) < MAXT1))
        {
            voxelValue = -1.0 * tr / tempValue;
        } else {
            voxelValue = 0.0;
        }

        if (error && verbose)
        {
            cout << "Error on slice " << i / numVoxels[0] * numVoxels[1] + 1 << endl;
        }
        T1Array->SetTuple1(i, (int)(voxelValue));
    }

    // Write out probability map
    // If the type is supported be svkImageWriterFactory then use it, otherwise use the vtkXMLWriter

    svkImageWriterFactory* writerFactory = svkImageWriterFactory::New();
    svkImageWriter*        writer        = static_cast<svkImageWriter*>(writerFactory->CreateImageWriter(dataTypeOut));

    if (writer == NULL) {
        cerr << "Can not determine writer of type: " << dataTypeOut << endl;
        exit(1);
    }

    writerFactory->Delete();
    writer->SetFileName(outputFileName.c_str());
    writer->SetInput(T1);
    writer->Write();
    writer->Delete();

    readerPre->Delete();
    readerPost->Delete();
    T1->Delete();

    return 0; 
}
